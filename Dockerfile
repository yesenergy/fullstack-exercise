FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /server
WORKDIR /server
COPY server/requirements.txt /server/
RUN pip install -r requirements.txt
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
	&& apt-get -y install postgresql-client
COPY server /server/
COPY frontend /frontend/ 
